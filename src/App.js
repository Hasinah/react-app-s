import React, { useContext, useState } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider"; //j'importe MuiThemeProvider pour stylé mon app
import createMuiTheme from "@material-ui/core/styles/createMuiTheme"; //j'importe pour crée la Theme function Theme

//ici j'import mes pages
import Home from "./components/Home";
import Login from "./components/Login";
import Register from "./components/Register";
import Navbar from "./components/Navbar";
import Post from "./components/Post";
import UserContext from "./contexts/UserContext";

//je crée la function theme dont j'ai fais l'import en haut et je lui fais passé un objet
const Theme = createMuiTheme({
  palette: {
    primary: {
      light: "#9266ff",
      main: "#9266ff",
      dark: "#008394",
      contrastText: "#fff",
      margin: "15px",
    },
    secondary: {
      light: "#ff6333",
      main: "#ff3d00",
      dark: "#b22a00",
      contrastText: "#fff",
    },

    typography: {
      useNextVariants: true,
    },
  },
});

const App = () => {
  //j'appel mon context=> le jwt
  // const { jwt, user, setJwt, setUser } = useContext(UserContext);
  //==========suite à l'explication de Tim
  const [jwt, setJwt] = useState("");
  const [user, setUser] = useState([]);
  const [users, setUsers] = useState([]);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  //je défini le chemin avec les Route
  //MuiThemeProvider c'est pour le styles
  const value = {
    jwt,
    user,
    username,
    password,
    users,
    setJwt,
    setUser,
    setUsername,
    setPassword,
    setUsers,
  };

  return (
    <UserContext.Provider value={value}>
      <MuiThemeProvider theme={Theme}>
        {jwt}
        <div className="App">
          <Router>
            <div className="container">
              {jwt ? (
                <div>
                  <Navbar />
                  <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/post" component={Post} />
                  </Switch>
                </div>
              ) : (
                <Switch>
                  <Route exact path="/login" component={Login} />
                  <Route exact path="/Register" component={Register} />
                </Switch>
              )}
            </div>
          </Router>
        </div>
      </MuiThemeProvider>
    </UserContext.Provider>
  );
};

export default App;
