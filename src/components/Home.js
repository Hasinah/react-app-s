import React from "react";
import Post from "./Post";
import Users from "./Users";
import { Grid } from "@material-ui/core";
import { Fragment } from "react";
import Monprofile from "./Monprofile";

function Home() {
  return (
    <Fragment>
      <Grid container spacing={16}>
        <Grid item sm={9} xs={12}>
          <Post />
        </Grid>
        <Grid item sm={3} xs={12}>
          <Users />
          <Monprofile />
        </Grid>
      </Grid>
    </Fragment>
  );
}

export default Home;
