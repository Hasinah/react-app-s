import React, { useContext, useEffect, useState } from "react";
import axios from "axios";
import UserContext from "../contexts/UserContext";
export default function Users() {
  const [posts, setPosts] = useState([]);
  const { jwt, user, users, setJwt, setUser, setUsers } = useContext(
    UserContext
  );

  useEffect(() => {
    // console.log(jwt);
    axios
      .get("https://strapi-crea.5ika.org/users", {
        headers: { Authorization: `Bearer ${jwt}` },
      })
      .then((response) => {
        setUsers(response.data);
      });
  }, [jwt, setUsers]);
  // console.log(test);
  // affich posts et les personne qui ont posté
  return (
    <div>
      <h1>Les Users</h1>

      <ul>
        {users.map((user) => (
          <li>
            <p key={user.id}>{user.username}</p>
          </li>
        ))}
      </ul>
    </div>
  );
}
