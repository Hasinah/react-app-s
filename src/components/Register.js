import React, { useEffect, useContext, useState } from "react";
import { useHistory, Link } from "react-router-dom";
import { Grid } from "@material-ui/core";
import axios from "axios";
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import UserContext from "../contexts/UserContext";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";

const styles = {
  form: {
    textAlign: "center",
  },
  button: {
    marginTop: "50px",
  },
};

const Register = () => {
  const [classes, setClasses] = useState("");
  const [lastname, setLastName] = useState("");
  const [firstname, setFirstName] = useState("");
  const [bio, setBio] = useState("");
  const [email, setEmail] = useState("");
  const {
    jwt,
    user,
    setJwt,
    setUser,
    username,
    password,
    setUsername,
    setPassword,
  } = useContext(UserContext);
  // const [error, seteError] = useState("");

  const [role, setRole] = useState("");
  let history = useHistory();

  const getRegister = () => {
    axios
      .post("https://strapi-crea.5ika.org/auth/local/register", {
        username: username,
        email: email,
        password: password,
        role: role,
        firstname: firstname,
        lastname: lastname,
        bio: bio,
      })
      .then((response) => {
        setJwt(response.data.jwt);
        setUser(response.data.user);

        // permet de sauvgarder
        localStorage.setItem("jwt", JSON.stringify(jwt));

        history.push("/");

        // getUsers();
      });
  };
  const onSubmit = (e) => {
    e.preventDefault();

    getRegister();
  };

  //mon grid de main et asid avec un  espace entre  de 16px
  return (
    <div>
      <AppBar>
        <Toolbar className="nav-container">
          <Typography variant="h3" className={classes.pageTitle}>
            Register
          </Typography>
        </Toolbar>
      </AppBar>
      <Grid container className={classes.form}>
        <Grid item sm />
        <Grid item sm>
          <form>
            <TextField
              id="username"
              name="username"
              type="text"
              label=" User Name"
              className={classes.textField}
              // helperText={errors.name}
              value={username}
              onChange={(e) => setUsername(e.target.value)}
              fullWidth
            />
            <TextField
              id="email"
              name="email"
              type="email"
              label="Email"
              // className={classes.textField}
              // helperText={errors.email}
              // error={errors.email ? true : false}
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              fullWidth
            />
            <TextField
              id="firstname"
              name="firstname"
              type="text"
              label=" First Name"
              className={classes.textField}
              // helperText={errors.name}
              value={firstname}
              onChange={(e) => setFirstName(e.target.value)}
              fullWidth
            />
            <TextField
              id="lastname"
              name="lastname"
              type="text"
              label="Last Name"
              className={classes.textField}
              // helperText={errors.name}
              value={lastname}
              onChange={(e) => setLastName(e.target.value)}
              fullWidth
            />
            <TextField
              id="password"
              name="password"
              type="password"
              label="Password"
              className={classes.textField}
              // helperText={errors.password}
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              fullWidth
            />
            <TextField
              id="bio"
              name="bio"
              type="text"
              label="Bio"
              className={classes.textField}
              // helperText={errors.password}
              value={bio}
              onChange={(e) => setBio(e.target.value)}
              fullWidth
            />
            <TextField
              id="rol"
              name="rol"
              type="text"
              label="Rol"
              className={classes.textField}
              // helperText={errors.password}
              value={role}
              onChange={(e) => setRole(e.target.value)}
              fullWidth
            />

            {/* {errors.general && (
              <Typography variant="body2" className={classes.customError}>
                {errors.general}
              </Typography>
            )} */}
            <Button
              type="submit"
              variant="contained"
              color="primary"
              onClick={onSubmit}
            >
              REGISTER
            </Button>
            <Button className="link" variant="contained" color="primary">
              <Link to="/login"> se connecter</Link>
            </Button>
            <br />
          </form>
        </Grid>
        <Grid item sm />
      </Grid>
    </div>
  );
};
Register.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Register);
