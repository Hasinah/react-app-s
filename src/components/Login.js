import React, { useEffect, useContext, useState } from "react";
import { useHistory, Link } from "react-router-dom";
import { Grid } from "@material-ui/core";
import axios from "axios";
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import UserContext from "../contexts/UserContext";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import "../App.css";
// import { Fragment, useEffect, useState } from "react";
// import Login from "./login";

const styles = {
  form: {
    textAlign: "center",
  },
};

function Login() {
  // jwt pour est unique permet de savoir la connection
  // const [jwt, setJwt] = useState(""); // sont vide
  // const [user, setUser] = useState([]);
  const [classes, setClasses] = useState("");
  const {
    jwt,
    user,
    users,
    setJwt,
    setUser,
    username,
    password,
    setUsername,
    setPassword,
    setUsers,
  } = useContext(UserContext);
  const [error, setError] = useState("");
  // console.log(jwt);
  let history = useHistory();

  const getJwt = () => {
    axios
      .post("https://strapi-crea.5ika.org/auth/local", {
        identifier: username,
        password: password,
      })
      .then((response) => {
        setJwt(response.data.jwt);
        setUser(response.data.user);
        setPassword(response.data.password);
        setUsername(response.data.username);
        console.log(response);

        // permet de sauvgarder
        localStorage.setItem("jwt", JSON.stringify(response.data.jwt));

        history.push("/");

        // getUsers();
      })
      .catch((err) => setError(error));
  };
  useEffect(() => {
    //verifie  jwt et set
    const jwtlocalstorage = localStorage.getItem("jwt");
    if (jwtlocalstorage) {
      setJwt(JSON.parse(jwtlocalstorage));
    }
  }, [setJwt]);
  const onSubmit = (e) => {
    e.preventDefault();

    getJwt();
  };

  //mon grid de main et asid avec un  espace entre  de 16px
  return (
    <div>
      <AppBar>
        <Toolbar className="nav-container">
          <Typography variant="h3" className={classes.pageTitle}>
            Login
          </Typography>
        </Toolbar>
      </AppBar>

      <Grid container className={classes.form}>
        <Grid item sm />
        <Grid item sm>
          <form>
            {/* <TextField
            id="email"
            name="email"
            type="email"
            label="Email"
            // className={classes.textField}
            // helperText={errors.email}
            // error={errors.email ? true : false}
            value={this.state.email}
            onChange={(e) => setPassword(e.target.value)}
            fullWidth */}
            {/* /> */}
            <TextField
              id="name"
              name="name"
              type="text"
              label="Name"
              // className={classes.textField}
              // helperText={errors.name}

              value={username}
              onChange={(e) => setUsername(e.target.value)}
              fullWidth
            />
            <TextField
              id="password"
              name="password"
              type="password"
              label="Password"
              // className={classes.textField}
              // helperText={errors.password}

              value={password}
              onChange={(e) => setPassword(e.target.value)}
              fullWidth
            />
            {/* {errors.general && (
            <Typography variant="body2" className={classes.customError}>
              {errors.general}
            </Typography>
          )} */}
            <div className="button">
              <Button
                type="submit"
                variant="contained"
                color="primary"
                onClick={onSubmit}
              >
                {/* Login
            {loading && (
              <CircularProgress size={30} className={classes.progress} />
            )} */}
                LOGIN
              </Button>
              <Button className="link" variant="contained" color="primary">
                <Link to="/register"> Créer un Compte</Link>
              </Button>
            </div>

            <br />
          </form>
        </Grid>
        <Grid item sm />
      </Grid>
    </div>
  );
}
Login.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Login);
