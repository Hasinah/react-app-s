import React, { Fragment, useContext, useEffect, useState } from "react";
import axios from "axios";
import Typography from "@material-ui/core/Typography";
import UserContext from "../contexts/UserContext";
import CardContent from "@material-ui/core/CardContent";
import CardActionArea from "@material-ui/core/CardActionArea";
import Card from "@material-ui/core/Card";
import withStyles from "@material-ui/core/styles/withStyles";
const styles = {
  card: {
    display: "flex",
    marginBottom: 100,
  },
  // image: {
  //   minWidth: 200,
  // },
  content: {
    padding: 25,
    objectFit: "cover",
  },
  MuiCardActionArea: {
    margin: 100,
  },
};

export default function EditPost() {
  const [posts, setPosts] = useState([]);
  const { jwt, user, setJwt, setUser } = useContext(UserContext);

  useEffect(() => {
    // console.log(jwt);
    axios
      .POST("https://strapi-crea.5ika.org/posts", {
        headers: { Authorization: `Bearer ${jwt}` },
      })
      .then((response) => {
        setPosts(response.data);
      });
  }, [jwt, setPosts]);
  // console.log(test);
  // affich posts et les personne qui ont posté
  return (
    <div>
      <h1>Editer un Post</h1>

      {posts.map((post) => (
        <Card>
          <CardActionArea>
            <CardContent>
              <Typography
                gutterBottom
                variant="h5"
                component="h2"
                key={post.id}
              >
                {post.user.username}{" "}
              </Typography>
              <Typography
                gutterBottom
                variant="h5"
                component="h2"
                key={post.id}
              >
                {post.user.title}{" "}
              </Typography>
              <Typography
                gutterBottom
                variant="h5"
                component="h2"
                key={post.id}
              >
                {post.content}
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      ))}
    </div>
  );
}
